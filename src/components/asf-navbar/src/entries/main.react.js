// eslint-disable-next-line no-unused-vars
import React from 'react';
import ReactDom from 'react-dom';

import Navbar from '../react-components/Navbar';

ReactDom.render(
    React.createElement(Navbar),
    document.getElementById('app')
);
